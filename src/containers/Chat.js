import React, {Component, Fragment} from "react";
import InputForm from "../components/InputForm/InputForm";
import OnePost from "../components/OnePost/OnePost";
import {connect} from "react-redux";
import {getData, postData} from "../store/action";
import "./Chat.css";
class Chat extends Component {
    state = {
        author: "",
        message: "",
        postsArray: []
    };


    changeName = event => this.setState({author: event.target.value});

    changePost = event => this.setState({message: event.target.value});


    componentDidMount = () => {
        this.interval = setInterval(this.props.getData, 3000);

    };
    componentWillUnmount = () => {
        clearInterval(this.interval);
        console.log("Unmount");
        //Не отрабатывает в таком виде...
    };


    addPost = () => {

        this.props.postData({author: this.state.author, message: this.state.message})
        this.setState({
            message: ""
        })

    };


    render() {

        return (<Fragment>

                <div className="chatWindow">
                    {this.props.messages.map(post => {
                        return <OnePost author={post.author} post={post.message} time={post.date}
                                        key={post.id}/>
                    })}</div>
                <InputForm changeAuthor={(event => this.changeName(event))}
                           changeText={(event => this.changePost(event))} clickButton={this.addPost}
                           authorValue={this.state.author} postValue={this.state.message}
                           isEmptyAuthor={this.props.authorError}
                           isEmptyText={this.props.postError}/>

            </Fragment>
        );
    }
}
;

const mapStateToProps = state => {
    return {
        messages: state.messageArray,
        postError: state.postError,
        authorError: state.authorError


    }
};

const mapDispatchToProps = dispatch => {

    return {
        postData: (data) => dispatch(postData(data)),
        getData: () => dispatch(getData())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);