import {CORRECT_FIELDS, ERROR, FETCH, POST_ERR, SUCCESS} from "./action";
const initialState = {
        messageArray: [],
        loading: false,
        error: false,
        postError: false,
        authorError: false


    }
;
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH: {
            return {...state, loading: true, error: false, authorError: false, postError: false};
        }
        case SUCCESS: {

            return {
                ...state,
                loading: false,
                error: false,
                messageArray: action.message
            }
        }
        case POST_ERR: {
            if (JSON.parse(action.error).error === "Author must be present in the request")
                return {...state, loading: false, error: false, authorError: true, postError: false};
            else if (JSON.parse(action.error).error === "Message must be present in the request")
                return {...state, loading: false, error: false, authorError: false, postError: true};
            else {
                return state;
            }

        }
        case CORRECT_FIELDS: {
            return {...state, authorError: false, postError: false};
        }
        case ERROR: {
            return {...state, loading: false, error: true};
        }

        default:
            return state;
    }


};
export default reducer;