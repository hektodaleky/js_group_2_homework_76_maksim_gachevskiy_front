import axios from "../axios";
export const FETCH = 'FETCH';
export const SUCCESS = 'SUCCESS';
export const POST_ERR = 'POST_ERR';
export const ERROR = 'ERROR';
export const CORRECT_FIELDS = 'CORRECT_FIELDS';


export const getFetch = () => {
    return {type: FETCH}
};
export const fetchSuccess = (message) => {
    return {type: SUCCESS, message}
};
export const fetchPostErr = (error) => {
    return {type: POST_ERR, error}
};
export const fetchError = () => {
    return {type: ERROR}
};
export const setCorrectFields = () => {
    return {type: CORRECT_FIELDS}
}


export const postData = (data) => {
    const link = `/messages`;
    return dispatch => {


        axios.post(link, data).then(
            response => {
                dispatch(setCorrectFields());
                dispatch(getData());

            }, (error) => {
                dispatch(fetchPostErr(JSON.stringify(error.response.data)));
            }
        );

    }
};

export const getData = () => {
    const link = `/messages`;
    return dispatch => {


        axios.get(link).then(
            response => {
                dispatch(fetchSuccess(response.data));

            }
        ).catch((log) => {

        });

    }
};