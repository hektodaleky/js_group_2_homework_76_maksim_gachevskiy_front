import React from "react";
const OnePost = props => {
    return (<div className="oneBlock">
        <p className="auth">{props.author}</p>
        <p className="post">{props.post}</p>
        <p className="time">{props.time}</p>
    </div>)
};
export default OnePost;