import React from "react";
import "./InputForm.css";
const InputForm = props => {
    return (<div className="second-div">
        <div><label htmlFor="author">Author</label>
            <input className={`empty${props.isEmptyAuthor}`} onChange={props.changeAuthor} value={props.authorValue}
                   id="author" type="text"></input>
        </div>
        <div><label htmlFor="inp-text">Input Text</label>
            <textarea className={`empty${props.isEmptyText}`} onChange={props.changeText} value={props.postValue}
                      id="inp-text" type="text"></textarea>
        </div>
        <button onClick={props.clickButton} id="addPost">Add Post</button>
    </div>);
};
export default InputForm;